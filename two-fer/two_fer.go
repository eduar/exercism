// package twofer returns the string "One for X, one for me." When X is a name or "you".
package twofer

// sharewith receives a value of type string, which can be empty, and returns
// another string as a phrase that has the name passed in or the word "you" if no name is passed.
func ShareWith(name string) string {
	if name == "" {
		name = "you"
	}
	return "One for " + name + ", one for me."
}
