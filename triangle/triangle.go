// package triangle help us determine the type of a triangle.
package triangle

import "math"

type Kind int

const (
	// Pick values for the following identifiers used by the test program.
	NaT = iota // not a triangle
	Equ        // equilateral
	Iso        // isosceles
	Sca        // scalene
)

// KindFromSides give us the kind of triangle formed with the sides given if there is one.
func KindFromSides(a, b, c float64) Kind {
	var k Kind

	switch {
	case math.IsInf(a, 0) || math.IsInf(b, 0) || math.IsInf(c, 0):
		k = NaT
	case a/b == 1.0 && b/c == 1.0:
		k = Equ
	case (a/b == 1.0 && a+b >= c) || (b/c == 1.0 && b+c >= a) || (c/a == 1.0 && c+a >= b):
		k = Iso
	case a+b >= c && b+c >= a && c+a >= b && a > 0 && b > 0 && c > 0:
		k = Sca
	default:
		k = NaT
	}
	return k
}
