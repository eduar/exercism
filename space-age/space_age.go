package space

//Planet is a type with a planet name
type Planet string

const earthYear = 31557600.00

var system = map[Planet]float64{
	"Earth":   1,
	"Mercury": 0.2408467,
	"Venus":   0.61519726,
	"Mars":    1.8808158,
	"Jupiter": 11.86215,
	"Saturn":  29.447498,
	"Uranus":  84.016846,
	"Neptune": 164.79132,
}

//Age helps us caculating the age on any planet of our solar system.
func Age(seconds float64, planet Planet) float64 {
	return seconds / earthYear / system[planet]
}
