package hamming

import "errors"

// Distance gives us the hamming difference between two DNA strands and a error if there is an error.
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, errors.New("Strands of different length")
	} else {
		c := 0
		for i := 0; i < len(a); i++ {
			if a[i] != b[i] {
				c++
			}
		}
		return c, nil
	}
}
