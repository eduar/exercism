//Package leap helps us to know what is a leap year.
package leap

//IsLeapYear tells us if a year is a leap year or not
func IsLeapYear(y int) bool {
	if y%4 == 0 {
		if y%100 == 0 {
			if y%400 == 0 {
				return true
			}
			return false
		}
		return true
	}
	return false
}
